'100040721_1.jpg' and 'detect.jpg' are collected from [opencv_extra/testdata/cv/face](https://github.com/opencv/opencv_extra/tree/master/testdata/cv/face).

'messi5.jpg' is collected from [opencv/samples/data](https://github.com/opencv/opencv/tree/master/samples/data).

These images are licensed under their source licenses.
